# GeoModel - A Detector Description Toolkit for HEP experiments


The GeoModel Toolkit offers classes that provide geometrical primitives for describing detectors, and a set of tools for accessing, handling, manipulating, dumping, restoring, visualizing, inspecting, and debugging the detector geometry.

The external dependencies are minimal:

- The Eigen math library for the core packages
- The `Xerces-C` XML parser and the `nlohmann_json` JSON parser to import external data
- The Qt framework for the visualization

----

*This Documentation is Work In Progress*
